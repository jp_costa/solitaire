pub use super::error_kind::ErrorKind;
pub use super::error_utils::*;

#[derive(Debug)]
pub struct Error {
    kind: ErrorKind,
    error: String,
}

impl Error {
    pub fn new(kind: ErrorKind, error: String) -> Self {
        Self { kind, error }
    }

    pub fn kind(&self) -> &ErrorKind {
        &self.kind
    }

    pub fn error(&self) -> &str {
        self.error.as_str()
    }
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}. {}", self.kind, self.error)
    }
}

impl std::error::Error for Error {}
