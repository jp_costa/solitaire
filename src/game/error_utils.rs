use super::error;
use crate::Card;

pub fn get_invalid_idx_error(capacity: usize) -> error::Error {
    error::Error::new(
        error::ErrorKind::IndexOutOfBound,
        format!("Index must be < {} (max index)", capacity),
    )
}

pub fn get_invalid_order_error<'a>(card: &'a Card, other: &&Card) -> error::Error {
    get_invalid_move(format!("{} must be < {} by 1 step", card, other))
}

pub fn get_invalid_move(error: String) -> error::Error {
    error::Error::new(error::ErrorKind::InvalidMove, error)
}
