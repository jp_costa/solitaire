#[derive(Debug)]
pub enum ErrorKind {
    InvalidMove,
    IndexOutOfBound,
}

impl std::fmt::Display for ErrorKind {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let err_msg = match self {
            ErrorKind::InvalidMove => "Invalid move",
            ErrorKind::IndexOutOfBound => "Invalid Index",
        };
        write!(f, "{}", err_msg)
    }
}
