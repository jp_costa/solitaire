const RANK_RANGE: std::ops::RangeInclusive<u8> = 1..=13;

#[derive(Debug, Eq, PartialEq)]
pub struct Rank(u8);

impl Rank {
    pub fn new(given_rank: u8) -> Self {
        if RANK_RANGE.contains(&given_rank) == false {
            unreachable!("Invalid rank");
        }
        Self(given_rank)
    }
}

impl PartialOrd for Rank {
    fn lt(&self, other: &Self) -> bool {
        other.0.checked_sub(self.0) == Some(1)
    }

    fn gt(&self, other: &Self) -> bool {
        self.0.checked_sub(other.0) == Some(1)
    }

    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Rank {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.0.cmp(&other.0)
    }
}

impl std::fmt::Display for Rank {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self.0 {
                1 => 'A',
                11 => 'J',
                12 => 'Q',
                13 => 'K',
                _ => char::from_digit(self.0 as u32, 10).expect("Could not cast rank to char"),
            }
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn lt() {
        assert_eq!(false, Rank::new(1) < Rank::new(3));
        assert!(Rank::new(1) < Rank::new(2));
    }

    #[test]
    fn gt() {
        assert_eq!(false, Rank::new(13) > Rank::new(10));
        assert!(Rank::new(13) > Rank::new(12));
    }
}
