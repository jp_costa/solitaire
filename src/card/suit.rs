#[derive(Debug, PartialEq)]
pub enum Suit {
    Club,
    Diamond,
    Heart,
    Spade,
}

#[derive(PartialEq)]
enum Color {
    Black,
    Red,
}

impl Suit {
    pub fn has_same_color_as(&self, other: &Self) -> bool {
        self.color() == other.color()
    }

    fn color(&self) -> Color {
        match self {
            Self::Club | Self::Spade => Color::Black,
            Self::Diamond | Self::Heart => Color::Red,
        }
    }
}

impl std::fmt::Display for Suit {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let suit: char = match self {
            Suit::Club => '♣',
            Suit::Diamond => '♦',
            Suit::Heart => '♥',
            Suit::Spade => '♠',
        };
        write!(f, "{}", suit)
    }
}
