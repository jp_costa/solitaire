use crate::{
    card::{self, Card},
    game::{self, error},
};

const FOUDATION_CAPACITY: usize = 4;
const STACK_CAPACITY: usize = 13;

pub struct Foundation<'a> {
    stacks: Vec<Vec<&'a Card>>,
    suits: [&'a card::Suit; FOUDATION_CAPACITY],
}

impl<'a> Foundation<'a> {
    pub fn new() -> Self {
        Self {
            stacks: vec![Vec::<&'a Card>::with_capacity(STACK_CAPACITY); FOUDATION_CAPACITY],
            suits: [
                &card::Suit::Club,
                &card::Suit::Diamond,
                &card::Suit::Heart,
                &card::Suit::Spade,
            ],
        }
    }

    pub fn pop(&mut self, from: usize) -> game::Result<Option<&'a Card>> {
        game::check_index_with(0..FOUDATION_CAPACITY, &from)?;
        Ok(self.stacks.get_mut(from).map(|stack| stack.pop()).flatten())
    }

    pub fn push(&mut self, other: &'a Card, at: usize) -> game::Result<()> {
        game::check_index_with(0..FOUDATION_CAPACITY, &at)?;
        self.can_append((other, at))?;
        self.stacks.get_mut(at).map(|stack| stack.push(other));
        Ok(())
    }

    fn can_append(&self, item: (&'a Card, usize)) -> game::Result<()> {
        let (item_suit, item_rank) = item.0.get_suit_rank();
        self.check_suit_with(item, item_suit)?;
        self.check_rank_with(item, item_rank)
    }

    fn check_suit_with(&self, item: (&'a Card, usize), item_suit: &card::Suit) -> game::Result<()> {
        self.suits
            .get(item.1)
            .map(|suit| {
                Ok(if *suit != item_suit {
                    return Err(error::get_invalid_move(format!("Must be {}", suit)));
                })
            })
            .transpose()?;
        Ok(())
    }

    fn check_rank_with(&self, item: (&'a Card, usize), item_rank: &card::Rank) -> game::Result<()> {
        self.get_last_card(item.1).map_or_else(
            || item.0.is_ace(),
            |last_card| {
                Ok(if (last_card.get_suit_rank().1 < item_rank) == false {
                    return Err(error::get_invalid_order_error(item.0, last_card));
                })
            },
        )
    }

    fn get_last_card(&self, at: usize) -> Option<&&Card> {
        self.stacks.get(at).map(|stack| stack.last()).flatten()
    }
}

impl<'a> std::fmt::Display for Foundation<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut buf = String::from("Foudation: [");
        let mut index: usize = 0;
        for suit in &self.suits {
            buf.push_str(suit.to_string().as_str());
            buf.push_str(" : ");
            self.stacks.get(index).map(|stack| {
                stack
                    .last()
                    .map(|card| buf.push_str(card.to_string().as_str()))
                    .unwrap_or_else(|| buf.push_str("<empty>"))
            });
            buf.push(' ');
            index += 1;
        }
        write!(f, "{}]", buf)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn push_to_foundation() {
        let mut f = Foundation::new();
        let mut ace_of_spades = Card::new(1, card::Suit::Spade);
        let mut two_of_spades = Card::new(2, card::Suit::Spade);
        let mut tree_of_spades = Card::new(3, card::Suit::Spade);
        let two_of_club = Card::new(2, card::Suit::Club);
        ace_of_spades.to_face_up();
        two_of_spades.to_face_up();
        tree_of_spades.to_face_up();
        assert!(f.push(&two_of_spades, 4).is_err()); // Invalid idx
        assert!(f.push(&two_of_spades, 3).is_err()); // Must be ace
        f.push(&ace_of_spades, 3).unwrap();
        assert!(f.push(&two_of_club, 3).is_err()); // Invalid suit
        assert!(f.push(&tree_of_spades, 3).is_err()); // Invalid order
        assert!(f.push(&two_of_spades, 3).is_ok()); // Invalid idx
    }

    #[test]
    fn push_to_empty_foundation() {
        let mut f = Foundation::new();
        let mut king_of_hearts = Card::new(13, card::Suit::Heart);
        let mut ace_of_spades = Card::new(1, card::Suit::Spade);
        king_of_hearts.to_face_up();
        ace_of_spades.to_face_up();
        assert!(f.push(&king_of_hearts, 2).is_err()); // Must be ace
        assert!(f.push(&ace_of_spades, 2).is_err()); // Invalid suit
        assert!(f.push(&ace_of_spades, 3).is_ok()); // Invalid suit
    }

    #[test]
    fn pop_from_foundation() {
        let mut f = Foundation::new();
        let mut ace_of_spades = Card::new(1, card::Suit::Spade);
        ace_of_spades.to_face_up();
        assert!(f.pop(4).is_err()); // Invalid suit
        assert!(f.pop(3).unwrap().is_none()); // Empty list
        f.push(&ace_of_spades, 3).unwrap();
        assert_eq!(
            Some(ace_of_spades.get_suit_rank()),
            f.pop(3).unwrap().map(|c| c.get_suit_rank())
        );
    }
}
