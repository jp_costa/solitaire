fn main() {
    let mut ace_of_spades = solitaire::Card::new(1, solitaire::Suit::Spade);
    let mut two_of_spades = solitaire::Card::new(2, solitaire::Suit::Spade);
    let mut king_of_hearts = solitaire::Card::new(13, solitaire::Suit::Heart);
    let mut club_lady = solitaire::Card::new(12, solitaire::Suit::Club);
    ace_of_spades.to_face_up();
    two_of_spades.to_face_up();
    king_of_hearts.to_face_up();
    club_lady.to_face_up();

    let mut f = solitaire::Foundation::new();
    let mut s = solitaire::StockPile::new(vec![&king_of_hearts, &club_lady]);
    print_with_delay(format!("{}\t\t{}", s, f));
    f.push(&ace_of_spades, 3).unwrap();
    print_with_delay(format!("{}\t\t{}", s, f));
    f.push(&two_of_spades, 3).unwrap();
    print_with_delay(format!("{}\t\t{}", s, f));

    for _ in 0..3 {
        s.drawn_card();
        print_with_delay(format!("{}\t\t{}", s, f));
    }

    f.pop(3).unwrap();
    print_with_delay(format!("{}\t\t{}", s, f));
    println!();
}

fn print_with_delay(msg: String) {
    use std::io::Write;
    print!("\r{}", msg);
    std::io::stdout().flush().unwrap();
    std::thread::sleep(std::time::Duration::new(2, 0));
}
