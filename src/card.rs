use crate::game::{self, error};

pub use self::{rank::Rank, suit::Suit};

mod rank;
mod suit;

pub struct Card {
    rank: rank::Rank,
    suit: suit::Suit,
    is_facing_up: bool,
}

impl Card {
    pub fn new(rank: u8, suit: Suit) -> Self {
        Self {
            rank: rank::Rank::new(rank),
            suit,
            is_facing_up: false,
        }
    }

    pub fn get_suit_rank(&self) -> (&Suit, &Rank) {
        (&self.suit, &self.rank)
    }

    pub fn is_ace(&self) -> game::Result<()> {
        Ok(if self.rank != Rank::new(1) {
            return Err(error::get_invalid_move(String::from("Must be an ace")));
        })
    }

    pub fn is_king(&self) -> game::Result<()> {
        Ok(if self.rank != Rank::new(13) {
            return Err(error::get_invalid_move(String::from("Must be an ace")));
        })
    }

    pub fn is_facing_up(&self) -> bool {
        self.is_facing_up
    }

    pub fn to_face_up(&mut self) {
        self.is_facing_up = true;
    }
}

impl std::fmt::Display for Card {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.is_facing_up == false {
            return write!(f, "|? ?|");
        }
        write!(f, "|{} {}|", self.suit, self.rank,)
    }
}

#[cfg(test)]
mod tests {
    #[test]
    #[should_panic]
    fn invalid_rank() {
        super::Card::new(0, super::Suit::Club);
    }

    #[test]
    fn display() {
        let mut card = super::Card::new(1, super::Suit::Spade);
        assert_eq!("|? ?|", card.to_string());
        card.to_face_up();
        assert_eq!("|♠ A|", card.to_string());
    }

    #[test]
    fn suit_and_rank() {
        let card = super::Card::new(1, super::Suit::Spade);
        assert_eq!(
            (&super::Suit::Spade, &super::Rank::new(1)),
            card.get_suit_rank()
        );
    }
}
