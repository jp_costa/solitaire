pub mod error;
mod error_kind;
mod error_utils;
pub type Result<T> = std::result::Result<T, error::Error>;

pub fn check_index_with(range: std::ops::Range<usize>, item: &usize) -> self::Result<()> {
    Ok(if range.contains(item) == false {
        return Err(error::get_invalid_idx_error(range.end));
    })
}
